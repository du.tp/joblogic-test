import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
/* Reset CSS */
*, :after, :before {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  padding: 0;
  margin: 0;
  color: #000;
  font-family: Roboto, sans-serif;
  font-size: 14px;
  font-weight: 400;
  background-color: white;
  line-height: 24px;
}

a {
  transition: all 0.3s;
}

.text-center {
  text-align: center;
}

.text-left {
  text-align: left;
}

.text-right {
  text-align: right;
}

.cursor {
  cursor: pointer;
}

.hide {
  display: none !important;
}

.no-background {
  background-color: transparent !important;
}

.m-0 {
	margin: 0;
}

.p-0 {
  padding: 0;
}

.fs-14 {
	font-size: 14px;
}
`;
