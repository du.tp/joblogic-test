import isNumber from 'lodash/isNumber';
import isString from 'lodash/isString';
import styled from 'styled-components';
import { FlexProps } from './type';

const Flex = styled.div<FlexProps>`
  display: ${(props) => props?.display || 'flex'};
  flex-wrap: ${(props) => props?.flexWrap || 'nowrap'};
  ${(props) => {
    if (isNumber(props?.gap)) {
      return `gap: ${props.gap}px`;
    }
    if (isString(props?.gap)) {
      return `gap: ${props.gap}`;
    }
    return '';
  }};
  ${(props) => (props.cursor ? `cursor: ${props.cursor}` : '')};
  ${(props) => (props.flex ? `flex: ${props.flex}` : '')};
  ${(props) => (props.width ? `width: ${props.width}` : '')};
  ${(props) => (props.maxWidth ? `max-width: ${props.maxWidth}` : '')};
  ${(props) => (props.height ? `height: ${props.height}` : '')};
  ${(props) => (props.minHeight ? `min-height: ${props.minHeight}` : '')};
  ${(props) => (props.margin ? `margin: ${props.margin}` : '')};
  ${(props) => (props.padding ? `padding: ${props.padding}` : '')};
  ${(props) => (props.alignItems ? `align-items: ${props.alignItems}` : '')};
  ${(props) => (props.flexDirection ? `flex-direction: ${props.flexDirection}` : '')};
  ${(props) => (props.justifyContent ? `justify-content: ${props.justifyContent}` : '')};
`;

export { Flex };
