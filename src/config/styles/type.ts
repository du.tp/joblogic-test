export interface FlexProps {
  gap?: number | string;
  flex?: string;
  width?: string;
  maxWidth?: string;
  height?: string;
  minHeight?: string;
  margin?: string;
  padding?: string;
  display?: string;
  flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  alignItems?: 'inherit' | 'center' | 'flex-end' | 'flex-start' | 'stretch' | 'baseline';
  flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse' | 'inherit' | 'initial' | 'revert' | 'revert-layer' | 'unset';
  justifyContent?: 'inherit' | 'center' | 'flex-end' | 'flex-start' | 'space-around' | 'space-between' | 'space-evenly';
  cursor?: 'pointer' | 'not-allowed';
}
