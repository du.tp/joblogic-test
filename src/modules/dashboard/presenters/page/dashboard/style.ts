import { Flex, FlexProps } from "src/config/index";
import styled from "styled-components";

export const Wrapper = styled(Flex)`
  position: relative;
  z-index: 1;
  max-width: 1440px;
  margin: 0 auto;
`;
export const Banner = styled(Flex)`
  position: relative;
  z-index: 1;
  height: 100vh;
  background-color: #2d2e36;
`;

export const ShadownStyle = styled.span`
  position: absolute;
  z-index: 2;
  top: 270px;
  left: 0;
  width: 100%;
  height: 50px;
  background-color: #000;
  opacity: 0.6;
  box-shadow: 0 -26px 20px rgba(0, 0, 0, 1);
`;
export const WrapImg = styled(Flex)`
  position: relative;
  height: 320px;
  overflow: hidden;
  img {
    position: absolute;
    top: -130px;
    left: 0;
    z-index: 1;
    width: 100%;
    height: auto;
  }
`;
export const Content = styled(Flex)`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
  height: 100%;
  background-color: transparent;
`;
export const SideBar = styled(Flex)`
  max-width: 360px;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5);
`;
export const Detail = styled(Flex)`
  flex: 1;
  padding-top: 260px;
  padding-left: 76px;
  padding-right: 76px;
  overflow: hidden;
  overflow-y: auto;
`;
export const WrapLogo = styled(Flex)`
  width: 100%;
  height: 320px;
`;
export const Avatar = styled(Flex)`
  width: 180px;
  height: 180px;
  border-radius: 6px;
  border: 1px solid #ccc;
  img {
    width: 100%;
    height: 100%;
    border-radius: 6px;
  }
`;

export const EmployeesList = styled(Flex)`
  padding: 20px 0;
  color: #ffffff;
`;

export const EmployeeItem = styled(Flex)<FlexProps & { popularity?: number }>(
  (props) => `
  padding: 16px 12px;
  font-size: ${(props.popularity || 2) < 5 ? "24px" : "38px"};
  color: ${(props.popularity || 2) < 5 ? "#ffffff" : "#3AD2F9"};
  font-weight: 300;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  &.active {
    background-color: #404146;
    font-size: ${(props.popularity || 2) < 5 ? "28px" : "46px"};
  }
`
);
export const Name = styled.h2<{size: number}>(props => `
  color: #fff;
  font-size: ${props?.size || 32}px;
  font-weight: 300;
  padding-bottom: 24px;
`);

export const Biography = styled.h2`
  background-color: #1a1d24;
  border-radius: 8px;
  padding: 24px;
  p {
    font-size: 16px;
    line-height: 1.5;
    color: #fff;
    font-weight: 300;
  }
`;
export const Title = styled(Flex)`
  color: #fff;
  font-size: 24px;
  font-weight: 300;
  padding-bottom: 16px;
`;
export const TitleSize = styled(Flex)`
  color: #fff;
  font-size: 24px;
  font-weight: 300;
`;
export const CustomSize = styled(Flex)`
  padding-top: 32px;
  padding-bottom: 32px;

  input[type="range"] {
    -webkit-appearance: none;
    width: 100%;
    height: 6px;
    border-radius: 5px;
    background-color: #1A1D24;
    outline: none;

    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      width: 28px;
      height: 28px;
      border-radius: 50%;
      background-color: white;
      cursor: pointer;
    }

    &:active::-webkit-slider-thumb {
      background-color: lightgray;
    }
  }
`;
