import { Employee } from "../entities";

export interface DashboardState {
  employeeSelected: Employee;
}
