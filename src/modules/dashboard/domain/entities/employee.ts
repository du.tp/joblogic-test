export type Employee = {
  id?: string,
  name?: string,
  popularity?: number,
  biography?: string,
  image?: string,
  colleagues?: string[],
}
