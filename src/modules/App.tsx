import React from "react";
import { DashboardPage } from "./dashboard/presenters/page";
import { GlobalStyle } from "src/config";

const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />
      <DashboardPage />
    </>
  );
};

export default App;
